import { Dispatch, SetStateAction } from 'react'

export interface IProduct {
  _id: string
  name: string
  details: string
  price: number
  slug: {
    current: string
  }
  image: IImage[]
}

export interface IBanner {
  buttonText: string
  description: string
  discount: string
  image: IImage
  largeText1: string
  largeText2: string
  midText: string
  smallText: string
  product: string
  saleTime: string
}

export interface IContext {
  showCart: boolean
  cartItems: ICartProduct[]
  totalPrice: number
  totalQuantities: number
  qty: number
  incQty: () => void
  decQty: () => void
  onAdd: (product: IProduct, quantity: number) => void
  setShowCart: Dispatch<SetStateAction<boolean>>
  onRemove: (id: string) => void
  toggleCartItemQuantity: (id: string, value: 'inc' | 'dec') => void
  setCartItems: Dispatch<SetStateAction<ICartProduct[]>>
  setTotalPrice: Dispatch<SetStateAction<number>>
  setTotalQuantities: Dispatch<SetStateAction<number>>
}

export interface ICartProduct extends IProduct {
  quantity: number
}

interface IImage {
  asset: {
    _ref: string
  }
}
