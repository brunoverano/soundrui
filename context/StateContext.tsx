import { createContext, useContext, useState } from 'react'
import toast from 'react-hot-toast'
import { ICartProduct, IContext, IProduct } from '../interfaces'

const Context = createContext<IContext>({} as IContext)

interface StateProps {
  children: JSX.Element | JSX.Element[]
}
export const StateContext = ({ children }: StateProps) => {
  const [showCart, setShowCart] = useState(false)
  const [cartItems, setCartItems] = useState<ICartProduct[]>([])
  const [totalPrice, setTotalPrice] = useState<number>(0)
  const [totalQuantities, setTotalQuantities] = useState<number>(0)
  const [qty, setQty] = useState(1)

  let foundProduct: ICartProduct
  let index: number

  const onAdd = (product: IProduct, quantity: number) => {
    const checkProductInCart = cartItems.find(
      (item) => item._id === product._id
    )

    setTotalPrice((prev) => prev + product.price * quantity)
    setTotalQuantities((prev) => prev + quantity)

    if (checkProductInCart) {
      const updatedCartItems = cartItems.map((product) => {
        if (product._id === product._id)
          return {
            ...product,
            quantity: product.quantity + quantity,
          }
      })

      setCartItems(updatedCartItems as ICartProduct[])
    } else {
      setCartItems([...cartItems, { ...product, quantity }])
    }

    toast.success(`${qty} ${product.name} added to the card`)
  }

  const onRemove = (id: string) => {
    foundProduct = cartItems.find(
      (product) => product._id === id
    ) as ICartProduct

    const newCartItems = cartItems.filter((product) => product._id !== id)

    setCartItems(newCartItems)
    setTotalPrice((prev) => prev - foundProduct.price * foundProduct.quantity)
    setTotalQuantities((prev) => prev - foundProduct.quantity)
  }

  const toggleCartItemQuantity = (id: string, value: 'inc' | 'dec') => {
    foundProduct = cartItems.find(
      (product) => product._id === id
    ) as ICartProduct
    index = cartItems.findIndex((product) => product._id === id)

    let newCartItems = cartItems.filter((product) => product._id !== id)

    if (value === 'inc') {
      newCartItems.splice(index, 0, {
        ...foundProduct,
        quantity: foundProduct.quantity + 1,
      })
      setCartItems([...newCartItems])
      setTotalPrice((prev) => prev + foundProduct.price)
      setTotalQuantities((prev) => prev + 1)
    } else {
      if (foundProduct.quantity > 1) {
        newCartItems.splice(index, 0, {
          ...foundProduct,
          quantity: foundProduct.quantity - 1,
        })
        setCartItems([...newCartItems])
        setTotalPrice((prev) => prev - foundProduct.price)
        setTotalQuantities((prev) => prev - 1)
      }
    }
  }

  const incQty = () => {
    setQty((prev) => prev + 1)
  }

  const decQty = () => {
    setQty((prev) => {
      if (prev < 2) return 1
      return prev - 1
    })
  }

  return (
    <Context.Provider
      value={{
        showCart,
        cartItems,
        totalPrice,
        totalQuantities,
        qty,
        incQty,
        decQty,
        onAdd,
        setShowCart,
        onRemove,
        toggleCartItemQuantity,
        setCartItems,
        setTotalPrice,
        setTotalQuantities,
      }}
    >
      {children}
    </Context.Provider>
  )
}

export const useStateContext = () => useContext(Context)
