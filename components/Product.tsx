import Link from 'next/link'
import { IProduct } from '../interfaces'
import { urlFor } from '../lib/client'

interface Props {
  product: IProduct
}

const Product = ({ product }: Props) => {
  return (
    <div>
      <Link href={`/product/${product.slug.current}`}>
        <div className="product-card">
          <img
            src={urlFor(product.image[0]).url()}
            alt={product.name}
            width={250}
            height={250}
            className="product-image"
          />
          <p className="product-name">{product.name}</p>
          <p className="product-price">${product.price}</p>
        </div>
      </Link>
    </div>
  )
}

export default Product
