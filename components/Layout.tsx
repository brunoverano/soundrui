import Head from 'next/head'
import { Navbar, Footer } from '../components'

interface Props {
  children: JSX.Element | JSX.Element[]
}

const Layout = ({ children }: Props) => {
  return (
    <>
      <Head>
        <title>Soundr</title>
      </Head>
      <div className="layout">
        <header>
          <Navbar />
        </header>
        <main className="main-container">{children}</main>
        <Footer />
      </div>
    </>
  )
}

export default Layout
