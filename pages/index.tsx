import { client } from '../lib/client'
import { FooterBanner, HeroBanner, Product } from '../components'
import { IBanner, IProduct } from '../interfaces'

interface Props {
  products: IProduct[]
  banner: IBanner
}

const Home = ({ products, banner }: Props) => {
  return (
    <>
      <HeroBanner banner={banner} />
      <div className="products-heading">
        <h2>Best Selling Products</h2>
        <p>Speakers of many variations</p>
      </div>
      <div className="products-container">
        {products.map((product) => (
          <Product key={product.name} product={product} />
        ))}
      </div>

      <FooterBanner banner={banner} />
    </>
  )
}

export const getServerSideProps = async () => {
  const productQuery = '*[_type == "product"]'
  const bannerQuery = '*[_type == "banner"]'

  const [products, banner] = await Promise.all([
    client.fetch(productQuery),
    client.fetch(bannerQuery),
  ])

  return {
    props: {
      products,
      banner: banner[0],
    },
  }
}

export default Home
