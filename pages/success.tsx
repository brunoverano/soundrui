import { useEffect } from 'react'
import { useStateContext } from '../context/StateContext'
import { BsBagCheckFill } from 'react-icons/bs'
import Link from 'next/link'
import { runFireworks } from '../lib/utils'

const success = () => {
  const { setCartItems, setTotalPrice, setTotalQuantities } = useStateContext()

  useEffect(() => {
    localStorage.clear()
    setCartItems([])
    setTotalPrice(0)
    setTotalQuantities(0)
    runFireworks()
  }, [])

  return (
    <div className="success-wrapper">
      <div className="success">
        <p className="icon">
          <BsBagCheckFill />
        </p>
        <h2>Thank you for your order!</h2>
        <p className="email-msg">Check you email inbox for the receipt.</p>
        <p className="description">
          If you have any questions, please email
          <a href="mailto:contact@soundr.com" className="email">
            contact@soundr.com
          </a>
        </p>
        <Link href="/">
          <button className="btn" type="button">
            Continue Shopping
          </button>
        </Link>
      </div>
    </div>
  )
}

export default success
